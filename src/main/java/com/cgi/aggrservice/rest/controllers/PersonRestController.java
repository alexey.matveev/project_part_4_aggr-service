package com.cgi.aggrservice.rest.controllers;

import com.cgi.aggrservice.api.PersonDto;
import com.cgi.aggrservice.service.impl.PersonService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/persons")
public class PersonRestController {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }


  //  @GetMapping(path = "/weird-calling-another-microservice/{id}")
    @GetMapping(path = "/{id}")
    public ResponseEntity<PersonDto> getPersonByIdWeird(@PathVariable("id") Long id) {

        return ResponseEntity.ok(personService.getPersonByIdFromMicroservice(id));
    }

}
