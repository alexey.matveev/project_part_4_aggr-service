package com.cgi.aggrservice.rest.controllers;

import com.cgi.aggrservice.api.TicketJiraDto;
import com.cgi.aggrservice.service.impl.JiraSoapService;
import com.ticket_jira_soap.ws.GetTicketByIdResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;
//Unsuccess
@RestController
@RequestMapping(path = "/jiraticket")
public class TicketJiraRestController {

    private JiraSoapService jiraSoapService;

    @Autowired
    public TicketJiraRestController(JiraSoapService jiraSoapService) {
        this.jiraSoapService = jiraSoapService;
    }
    //Unsuccess
    @GetMapping
    @ApiOperation(value = "Gets specific ticket by id from JiraService", response = TicketJiraDto.class)
    public ResponseEntity<Object> getTicketByIdFromJira(@RequestParam(value = "id", required = false, defaultValue = "-1") Long id  ) {

            GetTicketByIdResponse response = jiraSoapService.getTicketById(id);

          //  TicketJiraDto ticket = new TicketJiraDto(response.getTicket());
        //    return ResponseEntity.ok().body(ticket);
            return null;
        }

    //Unsuccess
    @GetMapping
    @ApiOperation(value = "Gets specific ticket by id or name attribute (nither both of theme) from JiraService", response = TicketJiraDto.class)
    public ResponseEntity<Object> getTicketByNameFromJira(@RequestParam(value = "name", required = false, defaultValue = "") String name) {


        //          return ResponseEntity.ok().body(new TicketJiraDto(jiraClient.getTicketByName(name).getTicket()));
        return null;
    }




}
