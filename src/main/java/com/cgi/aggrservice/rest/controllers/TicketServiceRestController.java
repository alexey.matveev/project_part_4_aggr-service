package com.cgi.aggrservice.rest.controllers;


import com.cgi.aggrservice.api.TicketServiceDto;
import com.cgi.aggrservice.service.impl.TicketServiceNowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/tickets_service")
public class TicketServiceRestController {

    private TicketServiceNowService ticketServiceNowService;

    @Autowired
    public TicketServiceRestController(TicketServiceNowService ticketServiceNowService) {
        this.ticketServiceNowService = ticketServiceNowService;
    }


    //   @GetMapping(path = "/calling-tickets-service-microservice/{id}")
    @GetMapping(path = "/{id}")
    public ResponseEntity<TicketServiceDto> getTicketByIdWeird(@PathVariable("id") Long id) {

        return ResponseEntity.ok(ticketServiceNowService.getTicketServiceByIdFromServiceNow(id));
    }

    //   @GetMapping(path = "/calling-tickets-service-microservice/name/{name}")
    @GetMapping(path = "/name/{name}")
    public ResponseEntity<TicketServiceDto> getTicketByNameWeird(@PathVariable("name") String name) {

        return ResponseEntity.ok(ticketServiceNowService.getTicketServiceByNameFromServiceNow(name));
    }

    @GetMapping(path = "/calling-tickets-service-microservice/")
    public ResponseEntity<TicketServiceDto> getAllTicketWeird() {
        System.out.println("Print URI from Person Controller: " );
return null;
      //  return ResponseEntity.ok(ticketServiceNowService.getAllTicketServiceFromServiceNow());
    }


}
