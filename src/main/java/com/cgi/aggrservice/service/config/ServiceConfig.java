package com.cgi.aggrservice.service.config;

import com.cgi.aggrservice.api.PersonDto;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.jms.Destination;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJms
public class ServiceConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

    @Bean
    public Destination personsQueue() {
        return new ActiveMQQueue(
                "servicenow.tickets.queue");
    }



    private void setTypeMappings(MappingJackson2MessageConverter messageConverter) {
        Map<String, Class<?>> typeIdMappings = new HashMap<>();
        typeIdMappings.put("person", PersonDto.class);
        messageConverter.setTypeIdMappings(typeIdMappings);
    }

}

