package com.cgi.aggrservice.service.config;

import com.cgi.aggrservice.service.impl.JiraSoapService;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class SoapConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.cgi.jira.ws");
        return marshaller;
    }
    @Bean
    public JiraSoapService jiraClient(Jaxb2Marshaller marshaller) {
        JiraSoapService client = new JiraSoapService();
        client.setDefaultUri("http://localhost:8082/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
