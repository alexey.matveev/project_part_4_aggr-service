package com.cgi.aggrservice.service.impl;

import com.cgi.aggrservice.api.PersonDto;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Transactional
public class PersonService {


    private RestTemplate restTemplate;

    @Autowired
    public PersonService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    String URL_USER_MANAGEMENT = "http://localhost:8061/user-management/persons/";

    //metod to microservice
    //
    public PersonDto getPersonByIdFromMicroservice(Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<PersonDto> personDto = restTemplate
                .exchange(URL_USER_MANAGEMENT + id,
                        HttpMethod.GET,
                        httpEntity,
                        PersonDto.class
                );


        return personDto.getBody();
    }

}
