package com.cgi.aggrservice.service.impl;


import com.cgi.aggrservice.api.TicketServiceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;



@Service
@Transactional
public class TicketServiceNowService {


    private RestTemplate restTemplate;

    @Autowired
    public TicketServiceNowService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    //metod to microservice
    //
    String URL_SERVICE_TICKET = "http://localhost:8083/service-now-rest/tickets/";

    public TicketServiceDto getTicketServiceByIdFromServiceNow(Long idTicket) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<TicketServiceDto> ticketServiceDto = restTemplate
                .exchange(URL_SERVICE_TICKET + idTicket,
                        HttpMethod.GET,
                        httpEntity,
                        TicketServiceDto.class
                );

        return ticketServiceDto.getBody();
    }

    public TicketServiceDto getTicketServiceByNameFromServiceNow(String name) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<TicketServiceDto> ticketServiceDto = restTemplate
                .exchange(URL_SERVICE_TICKET + "/name/" + name,
                        HttpMethod.GET,
                        httpEntity,
                        TicketServiceDto.class
                );
        // check for results
        return ticketServiceDto.getBody();
    }

//    public Page<TicketServiceDto> getAllTicketServiceFromServiceNow(Pageable pageable) {
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpEntity httpEntity = new HttpEntity(httpHeaders);
//        System.out.println("1. Print URI from Ticket-service: "  +  restTemplate.getUriTemplateHandler());

//        ResponseEntity<Page<TicketServiceDto>> ticketServiceDto = restTemplate
//                .exchange("http://localhost:8083/service-now-rest" + "/tickets" + "/" ,
//                        HttpMethod.GET,
//                        httpEntity,
//                        TicketServiceDto.class
//                );
        // check for results
    //    return ticketServiceDto.getBody();
//        return null;
//    }
}
