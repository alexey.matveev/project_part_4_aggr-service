package com.cgi.aggrservice.service.impl;


import com.ticket_jira_soap.ws.GetTicketByIdRequest;
import com.ticket_jira_soap.ws.GetTicketByIdResponse;
import com.ticket_jira_soap.ws.GetTicketByNameRequest;
import com.ticket_jira_soap.ws.GetTicketByNameResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

@Service
@Transactional
public class JiraSoapService extends WebServiceGatewaySupport {

    private String JIRA_SERVICE_ADDRESS = "http://localhost:8082/jira-soap/";


    public GetTicketByIdResponse getTicketById(long id) {
        GetTicketByIdRequest request = new GetTicketByIdRequest();
        request.setId(id);

        GetTicketByIdResponse response = (GetTicketByIdResponse) getWebServiceTemplate()
                .marshalSendAndReceive(JIRA_SERVICE_ADDRESS, request,
                        new SoapActionCallback(""));
        return response;
    }

    public GetTicketByNameResponse getTicketByName(String name) {
        GetTicketByNameRequest request = new GetTicketByNameRequest();
        request.setName(name);

        GetTicketByNameResponse response = (GetTicketByNameResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8082/ws/", request,
                        new SoapActionCallback(""));
        return response;
    }
}

