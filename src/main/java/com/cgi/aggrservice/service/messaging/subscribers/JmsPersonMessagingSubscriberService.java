package com.cgi.aggrservice.service.messaging.subscribers;

import com.cgi.aggrservice.api.TicketServiceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service
public class JmsPersonMessagingSubscriberService {

    private JmsTemplate jms;
    private Destination destination;

    @Autowired
    public JmsPersonMessagingSubscriberService(JmsTemplate jms, Destination destination) {
        this.jms = jms;
        this.destination = destination;
    }

    public TicketServiceDto subscribeTicketService() {
        TicketServiceDto message = (TicketServiceDto) jms.receiveAndConvert(destination);
        System.out.println("New message received ...: " + message);
        return message;
    }

}
