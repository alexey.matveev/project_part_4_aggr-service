package com.cgi.aggrservice.service.messaging.subscribers;

import com.cgi.aggrservice.api.TicketServiceDto;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class JmsPersonMessagingSubscriberListenerService {

    @JmsListener(destination = "servicenow.tickets.queue")
    public void subscribeTicketService(TicketServiceDto ticketServiceDto) {
        System.out.println("JMS listener.." + ticketServiceDto);
    }

}
