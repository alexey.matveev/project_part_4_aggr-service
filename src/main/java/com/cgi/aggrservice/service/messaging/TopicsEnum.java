package com.cgi.aggrservice.service.messaging;

public enum TopicsEnum {
    PERSONS_TOPIC("servicenow.tickets.queue");

    private String destination;

    TopicsEnum(String destination){
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
