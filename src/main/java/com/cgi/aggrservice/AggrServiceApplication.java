package com.cgi.aggrservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AggrServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AggrServiceApplication.class, args);
    }

}
